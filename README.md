# Qual versão do node devo rodar o projeto ?
O projeto precisa ser rodado na versão `v14.18.3`


# Como rodar essa aplicação

Uma vez feita a clonagem do repositorio e configurada a versão do node você deve rodar o comando `yarn` ou `npm install` para instalar todas as dependencias abaixo

```json
{
"hookform/resolvers": "^2.9.7",
"react-redux": "^8.0.2",
"react-router-dom": "^6.3.0",
"styled-components": "^5.3.5",
"yup": "^0.32.11",
"react-toastify": "^9.0.8",
}
```

Após ter feito toda está preparação você podera rodar o projeto no localhost:300 utilizando o comando `yarn start` e utilizar a aplicação na sua maquina 
