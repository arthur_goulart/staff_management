import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  background: #bdc3c7;
  flex-flow: column;
  align-items:center;
  background: linear-gradient(to right, #2c3e50, #bdc3c7);
`;

export const Title = styled.h1`
text-align:center;
`;

export const Card = styled.div`
  width: 1369px;
  display: flex;
  height:137px;
  margin-top:34px;
  justify-content: space-between;
  border: 1px solid;
`;


export const InfoBlock = styled.div `
  border: 1px solid;
  text-align:center;
  width: 185px;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: space-evenly;

`;


export const StaffInfo = styled.div``;



export const ActionBlock = styled.div `
  display: flex;
  width: 123px;
  flex-flow: column;
  justify-content: space-around;
  align-items: center;
`;

export const UpdateButton = styled.button `
    border: none;
    border-radius: 5px;
    width: 87px;
    height: 51px;
`;


