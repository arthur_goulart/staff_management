import { Container,Title,Card,InfoBlock ,StaffInfo,ActionBlock,UpdateButton} from "./styles";
import {  useState } from "react";
import StaffUpdateModal from "../UpdateModal";
import { useSelector } from "react-redux";
import { deleteStaff } from "../../store/modules/Staff/action";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
export const CardComponent = () =>{

    const dispatch = useDispatch()
    const workers = useSelector((state)=>state.workers)
    const [openModal,SetOpenModal] = useState(false)
    const [currentWorker,SetCurrentWorker] = useState()

    const HandleModal = ()=>{
        
        if(openModal === true){
            SetOpenModal(false)
        }
        if(openModal === false){
            SetOpenModal(true)
        }
    }

   
    const HandleDeleteStaff = (worker)=>{
        
        const Filtered = workers.filter((element)=>{return element !== worker})
        
        dispatch(deleteStaff(Filtered))
        toast.success("Employee Deleted Succefully")
    }
   

    return(<>
    <Container>
        <Title>Your Employees</Title>
        
        {workers?.map((value,index,array)=>(
        <Card key={index}>
        <InfoBlock>
            name
            <StaffInfo>
                {value.nome}
            </StaffInfo>
        </InfoBlock>
        <InfoBlock>
            cpf
        <StaffInfo>
                {value.cpf}
        </StaffInfo>
        </InfoBlock>
        <InfoBlock>
            salario          
            <StaffInfo>
                {value.salario}
        </StaffInfo>
        </InfoBlock>
        <InfoBlock>
            desconto
            <StaffInfo>
                {value.desconto}
            </StaffInfo>
        </InfoBlock>
        <InfoBlock>
            dependentes
            <StaffInfo>
                {value.dependentes}
            </StaffInfo>
        </InfoBlock>
        <InfoBlock>
            Desconto IRPF
            <StaffInfo>
                {value.irpf}
            </StaffInfo>
        </InfoBlock>
        <ActionBlock>

        <UpdateButton onClick={()=>{HandleModal(); SetCurrentWorker(index)}}>Update Employee info</UpdateButton>
        <UpdateButton onClick={()=> HandleDeleteStaff(value)}>Delete Employee</UpdateButton>
        </ActionBlock>
        </Card>
        ))
        }
        {openModal ? <StaffUpdateModal index={currentWorker} SetOpenModal={SetOpenModal}/>:<div></div>}
        
           
            
        
    </Container>
    </>)
}