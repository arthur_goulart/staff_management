import { NavigationHeader,ContentHeader,InteractiveDiv } from "./styles"
import { useNavigate } from "react-router-dom"

const HeaderComponent = ()=>{
    const navigate = useNavigate()
    return(
    <>
    <NavigationHeader>
        <ContentHeader>
        <InteractiveDiv onClick={()=>navigate('/')}>
                Home
            </InteractiveDiv>
            <InteractiveDiv onClick={()=>navigate('/staff-registration')}>
                Staff Registration
            </InteractiveDiv>
            <InteractiveDiv onClick={()=>navigate('/staff-list')}>
                Staff List
            </InteractiveDiv>
        </ContentHeader>
    </NavigationHeader>
    </>)
}

export default HeaderComponent