import styled from "styled-components";



export const NavigationHeader = styled.div`
    justify-content: center;
    display: flex;
    background: linear-gradient(to left, #240b36, #c31432);
    width:100%;
    position:fixed;
`;


export const ContentHeader = styled.div`
    height: 100px;
    width: 85%;
    justify-content: space-evenly;
    display: flex;
`;


export const InteractiveDiv = styled.div`

cursor:pointer;
font-weight:bolder;
justify-content: center;
align-items: center;
width: 254px;
display: flex;
`;