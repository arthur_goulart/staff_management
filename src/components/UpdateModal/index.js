import {  Content,Form } from "./styles";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { updateStaff } from "../../store/modules/Staff/action";
import { discountIRPF } from "../../utils/DiscountIRPF";
import { toast } from "react-toastify";
const StaffUpdateModal = ({index,SetOpenModal})=> {
  
  const dispatch = useDispatch()
  const workers = useSelector((state)=>state.workers)
   
    const schema = yup.object().shape({
        salary: yup
        .string()
        .required("This field is required")
        .matches(/^[0-9]*$/, "This field only accepts numbers"),
      
    
        discount: yup.string().required("This field is required"),
    
        dependants: yup.string().required("This field is required"),
    
      });
      const {
        register,
        handleSubmit,
        formState: { errors },
      } = useForm({ resolver: yupResolver(schema) });

    const Update = (data) => {
        const { salary,discount,dependants} = data

        const localData = workers.map((element)=>{return element})

        const nome = localData[index].nome
        const cpf = localData[index].cpf
        const StringtoNumber = Number(salary)
        const IRPF = discountIRPF(StringtoNumber,discount,dependants)
        const Newvalue = {nome,cpf,salario:StringtoNumber,desconto:Number(discount),dependentes:Number(dependants),irpf:IRPF}

        localData[index]= Newvalue
        

        SetOpenModal(false)
        
        dispatch(updateStaff(localData))
        toast.success("Employee Updated Succefully")
      };


    return (
        <Content>
        <Form onSubmit={handleSubmit(Update)}>
        <input
            className="salary"
            type="text"
            placeholder={workers[index].salario}
            {...register("salary")}
        />
          {errors.salary?.message}
        <input
            className="discount"
            type="text"
            placeholder={workers[index].desconto}
            {...register("discount")}
        />
        {errors.discount?.message}
        <input
            className="dependants"
            type="number"
            placeholder={workers[index].dependentes}
            {...register("dependants")}
        />
        {errors.dependants?.message}
        <button type="submit">
              Update Staff Info
        </button>
        </Form>
        </Content>
        )
}


export default StaffUpdateModal