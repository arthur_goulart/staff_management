import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  background: #bdc3c7;

  background: linear-gradient(to right, #2c3e50, #bdc3c7);
`;

export const Content = styled.div`
  width: 20%;
  display: flex;
  justify-content: center;
  height: 260px;
  top: 44px;
  right:100px;
  position: fixed;
  border-radius: 18px;
  align-items: center;
  background: wheat;
`;

export const Form = styled.form`
display:flex;
flex-flow:column;
justify-content: space-evenly;
height:190px;

`;