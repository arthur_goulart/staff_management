import {render, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import React from 'react';
import StaffRegistration from '../pages/Staff-Registration';

const mockedHandleSubmit = jest.fn();


jest.mock("react-hook-form", () => {
    return {
      useForm: () => ({
        handleSubmit: mockedHandleSubmit,
        errors: {
            name: "Letícia Aurora Farias",
            cpf: "936.938.039-60",
            salary:998,
            discount:74.85,
            dependants:2
        },
      }),
    };
  });
  

  describe("when submits the form", ()=>{
    test("Should call handleSubmit", async ()=>{
        render(<StaffRegistration/>);

        const nameInput = screen.getByTestId('name')
        const cpfInput = screen.getByTestId('cpf')
        const salaryInput = screen.getByTestId('salary')
        const discountInput = screen.getByTestId('discount')
        const dependantsInput = screen.getByTestId('dependants')

        userEvent.type(nameInput,"Letícia Aurora Farias")
        userEvent.type(cpfInput,"936.938.039-60")
        userEvent.type(salaryInput,'998')
        userEvent.type(discountInput,"74.85")
        userEvent.type(dependantsInput,"2")
        
    })
  })