export const CpfFormatation = (nonFormatedCpf)=> {

    const formattedString = nonFormatedCpf.slice(0,3)+"."+
                          nonFormatedCpf.slice(3,6)+"."+
                          nonFormatedCpf.slice(6,9)+"-"+
                          nonFormatedCpf.slice(9,11)
    return formattedString
  }