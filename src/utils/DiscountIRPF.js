export const discountIRPF = (salario,previdencia,dependentes)=>{
    const dependantsDeduction = 164.56


    if(salario === 1903.98){
        return salario
      }
    if(salario === 1903.99 || salario <= 2826.65){
      const BaseIR = salario - previdencia - dependantsDeduction * dependentes
      const IRPF = BaseIR * 7.5/100 - 142.80
      return IRPF
    }

    if(salario === 2826.66 || salario <= 3751.05){
      const BaseIR = salario - previdencia - dependantsDeduction * dependentes
      const IRPF = BaseIR * 15/100 - 354.80
      return IRPF
    }


    if(salario === 3751.06 || salario <= 4664.68){
      const BaseIR = salario - previdencia - dependantsDeduction * dependentes
      const IRPF = BaseIR * 22.5/100 - 636.13
      return IRPF
    }

    if(salario > 4664.68){
      const BaseIR = salario - previdencia - dependantsDeduction * dependentes
      const IRPF = BaseIR * 27.5/100 - 869.36
      return IRPF
    }

   return salario
  }