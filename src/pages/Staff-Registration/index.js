import { Container, Content,Form ,CustomButton} from "./styles";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addStaff } from "../../store/modules/Staff/action";
import { CpfFormatation } from "../../utils/FormattedString";
import { discountIRPF } from "../../utils/DiscountIRPF";
import HeaderComponent from "../../components/HeaderComponent";
import {toast} from 'react-toastify'
const StaffRegistration = ()=> {
  
  const dispatch = useDispatch();
  
    const schema = yup.object().shape({
        name: yup
        .string()
        .required("This field is required")
        .matches(/^[a-zA-Z\s]+$/, "This field only accepts letters"),
        cpf: yup
        .string()
        .min(11, "Type at least 11 caracteres")
        .required("This field is required")
        .matches(/^[0-9]*$/, "This field only accepts numbers"),
        salary: yup
        .string()
        .required("This field is required")
        .matches(/^[0-9]*$/, "This field only accepts numbers"),
      
    
        discount: yup.number().required("This field is required"),
    
        dependants: yup.number().required("This field is required"),
    
      });
      const {
        register,
        handleSubmit,
        formState: { errors },
      } = useForm({ resolver: yupResolver(schema) });



    const Registration = (data) => {
    const { name ,cpf,salary,discount,dependants} = data


    const formatted = CpfFormatation(cpf)
    const StringtoNumber = Number(salary)
    const IRPF = discountIRPF(StringtoNumber,discount,dependants)
    
      const newStaff = {nome:name,cpf:formatted,salario:StringtoNumber,desconto:discount,dependentes:dependants,irpf:IRPF}

       dispatch(addStaff(newStaff))
       toast.success("Employee Registered Succefully")
      };


    return (<>
    <HeaderComponent/>
    <Container>
        <Content>
        <h1>Registration</h1>
        <Form onSubmit={handleSubmit(Registration)}>
        <input
            data-testid="custom-element"
            className="name"
            type="text"
            placeholder="Name"
            {...register("name")}
        />
          {errors.name?.message}
        <input
            className="cpf"
            type="text"
            placeholder="CPF"
            {...register("cpf")}
        />
          {errors.cpf?.message}
        <input
            className="salary"
            type="text"
            placeholder="Salary"
            {...register("salary")}
        />
          {errors.salary?.message}
        <input
            className="discount"
            type="text"
            placeholder="Discount"
            {...register("discount")}
        />
        {errors.discount?.message}
        <input
            className="dependants"
            type="number"
            placeholder="Dependants"
            {...register("dependants")}
        />
        {errors.dependants?.message}
        <CustomButton type="submit">
              Criar Conta
        </CustomButton>
        <div className="link">
            <Link to="/staff-list">Staff List</Link>
            </div>
        </Form>
        </Content>
        </Container>
        </>)
}


export default StaffRegistration