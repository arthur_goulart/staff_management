import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  background: #bdc3c7;

  background: linear-gradient(to right, #2c3e50, #bdc3c7);
`;

export const Content = styled.div`
  width: 27%;
  display: flex;
  flex-direction:column;
  justify-content: center;
  h1{
  text-align:center
  }
`;

export const Form = styled.form`
display:flex;
flex-flow:column;
justify-content: space-evenly;
height:190px;

a{
  color:orange;
}

`;


export const CustomButton = styled.button`
border-radius: 5px;
cursor:pointer;
padding:10px;
font-size: 18px;
background: black;
color: white;
`;