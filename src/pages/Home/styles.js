import styled from "styled-components";

export const Container = styled.div`
  background: #bdc3c7;

  background: linear-gradient(to right, #2c3e50, #bdc3c7);

  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  height: 100vh;
`;

export const Content = styled.div`
  width: 50%;
  div {
    flex: 1;
    display: flex;
    margin-top: 1rem;
    justify-content: center;

    button {
      padding: 10px;
      border-radius: 7px;
      border: none;
      background: #4e54c8;
      background: linear-gradient(to right, #8f94fb, #4e54c8);
    }
    button + button {
      margin-left: 1rem;
    }
  }
`;
