
import { Container, Content } from "./styles";
import { useNavigate } from "react-router-dom";
import "./styles";

import HeaderComponent from "../../components/HeaderComponent";

const Home = () => {
  
const navigate  = useNavigate()

  return (
    <>
    <HeaderComponent/>
      <Container>
        <Content>
          <h1>Bem vindo</h1>
          <div>
            <button onClick={() => navigate('/staff-registration')}>
              Cadastro de funcionarios
            </button>
            <button onClick={() => navigate('/staff-list')}>
              Seus funcionarios
            </button>
          </div>
        </Content>
      </Container>
    </>
  );
};

export default Home;
