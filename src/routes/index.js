import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home";

import StaffRegistration from "../pages/Staff-Registration";

import StaffList from "../pages/Staff-list";


const RouterComponent = ()=>{
    return(
    
    <Routes>
        <Route exact path='/' element={<Home/>}/>
        <Route exact path='/staff-registration' element={<StaffRegistration/>}/>
        <Route exact path='/staff-list' element={<StaffList/>}/>
    </Routes>
    )
}


export default RouterComponent