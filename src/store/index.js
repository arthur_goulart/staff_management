import { legacy_createStore as createStore, combineReducers } from "redux";

import StaffReducer from "./modules/Staff/reducer";



const reducers = combineReducers({workers:StaffReducer});

const store = createStore(reducers);


export default store