

const StaffReducer =(state=[],action)=>{
    switch(action.type){
      case "ADD_STAFF":
        const { staff } = action;
        return [...state, staff]
      case "UPDATE_STAFF":
          const { payload } = action;

          return payload
      case "DELETE_STAFF":
            const { deleted } = action;
            return  deleted

        default:
            return state
    }
}


export default StaffReducer